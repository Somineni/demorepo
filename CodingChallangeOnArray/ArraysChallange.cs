﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallangeOnArray
{
    class ArraysChallange
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter the row size of the matrix");
            int rowSize = int.Parse(Console.ReadLine());

            Console.WriteLine("enter the column size of the matrix :");
            int colSize = int.Parse(Console.ReadLine());

            int[,] matrix = new int[rowSize, colSize];

            InsertElements(matrix, rowSize, colSize);

            InterChangeColumns(matrix, rowSize, colSize);

            for(int index=0;index<rowSize;index++)
            {
                for(int index1=0;index1<colSize;index1++)
                {
                    Console.Write(matrix[index, index1] + " ");
                }
                Console.WriteLine();
            }

            Console.ReadLine();
        }

        private static int[,] InterChangeColumns(int[,] matrix, int rowSize, int colSize)
        {
            for(int index=0;index<rowSize;index++)
            {
                int val1 = matrix[index, colSize - 1] * matrix[index, colSize - 1];
                int val2 = matrix[index, 0] * matrix[index, 0];
                matrix[index, 0] = val1;
                matrix[index, colSize - 1] = val2;
            }

            return matrix;
        }

        private static int[,] InsertElements(int[,] matrix, int rowSize, int colSize)
        {
            Console.WriteLine("enter the numbers into the array");
            for(int index=0;index<rowSize;index++)
            {
                for(int index1=0;index1<colSize;index1++)
                {
                    matrix[index, index1] = int.Parse(Console.ReadLine());
                }
            }
            return matrix;
        }
    }
}
